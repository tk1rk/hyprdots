
#!/bin/sh
# Times the screen off and puts it to background
swayidle \
    timeout 10 'hyprctl dispatch dpms off' \
    resume 'hyprctl dispatch dpms on' &

# Locks the screen immediately
swaylock \
    --screenshots \
    --clock \   
    --indicator-idle-visible \
    --inside-color 282a36 \
    --inside-clear-color 282a36 \
    --inside-ver-color 282a36 \
    --inside-wrong-color 282a36 \
    --key-hl-color bd93f9 \
    --line-color 282a36 \
    --line-clear-color 282a36 \
    --line-ver-color 282a36 \
    --line-wrong-color 282a36 \
    --ring-color 282a36 \
    --ring-clear-color 282a36 \
    --ring-ver-color 282a36 \
    --ring-wrong-color 282a36 \
    --separator-color bd93f9 \
    --text-color bd93f9 \
    --text-clear-color bd93f9 \
    --text-ver-color bd93f9 \
    --text-caps-lock-color bd93f9 \
    --text-wrong-color ff5555 \
    --effect-blur 4x2 \
    --text-clear Cleared \
    --text-caps-lock Maj \
    --text-ver Checking \
    --text-wrong Cheh \
    --bs-hl-color bd93f9 \
    --fade-in 0.5
    
#swaylock
# Kills last background task so idle timer doesn't keep running
kill %%
swaylock \ 	
    --screenshots \ 	
    --clock \ 	
    --indicator \ 	
    --indicator-radius 100 \ 	
    --indicator-thickness 7 \      
    --effect-blur 7x5 \ 	
    --effect-vignette 0.5:0.5 \ 	 
    --ring-color BD93F9 \ 	   
    --key-hl-color 6272A4 \ 	
    --line-color 00000000 \ 	
    --inside-color 00000088 \ 	   
    --separator-color 00000000 \ 	
    --grace 2 \ 	
    --fade-in 0.2