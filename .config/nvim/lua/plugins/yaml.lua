use {
  "someone-stole-my-name/yaml-companion.nvim",
  requires = {
      { "neovim/nvim-lspconfig" },
      { "nvim-lua/plenary.nvim" },
      { "nvim-telescope/telescope.nvim" }
  },
  config = {
  { local cfg = require("yaml-companion").setup({
    lspconfig = {
      cmd = {"yaml-language-server"}
    },
    }),
  require("lspconfig")["yamlls"].setup(cfg)
  },
  { 
  function() 
    require("telescope").load_extension("yaml_schema")
  end
  }
}