return {
    "Mofiqul/dracula.nvim",
    name = "dracula",
    priority = 1000,
    config = { 
      local dracula = require("dracula") dracula.setup() 
    },  
    show_end_of_buffer = true, 
    transparent_bg = true, 
    lualine_bg_color = "#282a36",
    italic_comment = true, 
    overrides = {},
}
    