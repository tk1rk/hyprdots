return {
  'VonHeikemen/lsp-zero.nvim',
  branch = 'v2.x',
  dependencies = {
    -- LSP Support
    {'neovim/nvim-lspconfig'},             
    {'williamboman/mason.nvim'},           
    {'williamboman/mason-lspconfig.nvim'}, 
    -- Autocompletion
    {'hrsh7th/nvim-cmp'},     
    {'hrsh7th/cmp-nvim-lsp'}, 
    {'L3MON4D3/LuaSnip'},    
  },
  config = {
    local lsp = require('lsp-zero').preset({}) 
      
    lsp.on_attach(function(client, bufnr) 
      -- see :help lsp-zero-keybindings 
      -- to learn the available actions 
      lsp.default_keymaps({buffer = bufnr}) 
    end) 
    
    -- (Optional) Configure lua language server for neovim   
    require('lspconfig').lua_ls.setup(lsp.nvim_lua_ls()) 

    lsp.setup()
  }
}
                                                                                                