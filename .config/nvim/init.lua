--Enable the new 'lua-loader' that byte-compiles and caches lua files.
vim.loader.enable()

-- load options,autocmds,keymaps,etc...
require("core")

-- bootstrap lazy.nvim
require("core.lazy")
