# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# unplugged
##? Clone a plugin, identify its init file, source it, and add it to your fpath. function plugin-load { local repo plugdir initfile initfiles=() : ${ZPLUGINDIR:=${ZDOTDIR:-~/.config/zsh}/plugins} for repo in $@; do plugdir=$ZPLUGINDIR/${repo:t} initfile=$plugdir/${repo:t}.plugin.zsh if [[ ! -d $plugdir ]]; then echo "Cloning $repo..." git clone -q --depth 1 --recursive --shallow-submodules \ https://github.com/$repo $plugdir fi if [[ ! -e $initfile ]]; then initfiles=($plugdir/*.{plugin.zsh,zsh-theme,zsh,sh}(N)) (( $#initfiles )) || { echo >&2 "No init file '$repo'." && continue } ln -sf $initfiles[1] $initfile fi fpath+=$plugdir (( $+functions[zsh-defer] )) && zsh-defer . $initfile || . $initfile done }

# plugins 
ZPLUGINDIR=$HOME/.zsh/plugins 
if [[ ! -d $ZPLUGINDIR/zsh-autosuggestions ]]; then 
    git clone https://github.com/zsh-users/zsh-autosuggestions \ 
    $ZPLUGINDIR/zsh-autosuggestions 
fi 
source $ZPLUGINDIR/zsh-autosuggestions/zsh-autosuggestions.plugin.zsh 

if [[ ! -d $ZPLUGINDIR/zsh-history-substring-search ]]; then 
    git clone https://github.com/zsh-users/zsh-history-substring-search \ 
    $ZPLUGINDIR/zsh-history-substring-search 
fi 
source $ZPLUGINDIR/zsh-history-substring-search/zsh-history-substring-search.plugin.zsh 
if [[ ! -d $ZPLUGINDIR/z ]]; then 
    git clone https://github.com/rupa/z \ 
$ZPLUGINDIR/z 
fi 
source $ZPLUGINDIR/z/z.sh

# kitty
if test -n "$KITTY_INSTALLATION_DIR"; then 
    export KITTY_SHELL_INTEGRATION="enabled" 
    autoload -Uz -- "$KITTY_INSTALLATION_DIR"/shell-integration/zsh/kitty-integration 
    kitty-integration 
    unfunction kitty-integration 
fi

source $ZDOTDIR/powerlevel10k/powerlevel10k.zsh-theme

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f $ZDOTDIR/.p10k.zsh ]] || source $ZDOTDIR/.p10k.zsh