# default Neovim config
alias n="nvim"
# LazyVim
alias nz="NVIM_APPNAME=nvim-lazyvim nvim" 
# nvchad 
alias nc="NVIM_APPNAME=nvim-nvchad nvim" 
# kickstart 
alias nk="NVIM_APPNAME=nvim-kickstart nvim"
# astrovim  
alias na="NVIM_APPNAME=nvim-astrovim nvim"  
# lunarvim
alias nl="NVIM_APPNAME=nvim-lunarvim nvim" 

alias ls="exactly -la --header --icons --no-time --group-directories-first --color=always --color-scale"
